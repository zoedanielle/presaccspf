
// Copyright (c) 2018 Santini Designs. All rights reserved.
// Zoe Stearns, 2022

#version 430 core
#define M_PI 3.14159265359

// Do not touch the code below
uniform float screenOffsetX;	    // Transform coordinate system into Eyeris
uniform float screenOffsetY;	    // Transform coordinate system into Eyeris
uniform float cX;	                // Center of object
uniform float cY;	                // Center of object
uniform float pixelAngle;


// User variable
uniform float circleRadius;
uniform float spf =  1.0; // cpd
uniform float phase = 0.0; // fraction of pi, should really always be 0 or 1
uniform float signalAmp = 0.1; // contrast scalar
uniform float noiseAmp = 0.3; // contrast of input image
uniform float orientation = 0.0; // orientation of the patch
uniform float theta = 0.0;
uniform float width = 2.0; // dva
uniform float alpha = 1.0; // for noise
uniform int flag = 0; // choose grating or grating + noise

// Fragment shader inputs/outputs
uniform sampler2D textureSampler;   // Texture sampler
in vec2 textureLocation;
out vec4 fragment;

///////////////////////////////////////////////////////////////////////////

float circle(float x, float y, float r,float width )
{

	float scale = 0.f;

	if (r < width )
	{

		//gaussian envelope
		float d = r * 60 / pixelAngle;
		float mean = 0; // this value should always be 0
		float gain = 1;
		float sd = width * 60/3;
		float env = gain*exp(-(d-mean)*(d-mean)/(2*sd*sd));

		scale = env;
	}

	return scale; //range [-1 1] if you remove +1 div by 2..sk - modify the scale such that black line is 255 and white is 0 in grating
}
///////////////////////// linear patch ///////////////////////////////


float patch_test(float x, float y, float r,float width,float angle, float phase)
{

	float scale = 0.f;

	if (r < width )
	{
		float wavelength = 60/spf;
		float phase = (x * cos(angle + M_PI/2) + y * sin(angle + M_PI/2)) / wavelength * 2 * M_PI;//put this back to place
		float sig = cos(phase);



		//gaussian envelope
		float d = r * 60 / pixelAngle;
		float mean = 0; // this value should always be 0
		float gain = 1;
		float sd = width *60/3; //3 times std dev aprox the radius of the envelope (width is radius here)
		float env = gain*exp(-(d-mean)*(d-mean)/(2*sd*sd));

		scale = sig*env;
	}

	return scale; //range [-1 1] if you remove +1 div by 2..sk - modify the scale such that black line is 255 and white is 0 in grating
}

///////////////////////////////////////////////////////////////////////////

void main()
{
    // Get coordinate
    float x = gl_FragCoord.x - (screenOffsetX + cX);
    float y = gl_FragCoord.y - (screenOffsetY + cY);
    float r = sqrt(x * x + y * y)* pixelAngle / 60;
    //float r = sqrt((x-(cX))*(x-(cX)) + (y-(cY)) * (y-(cY))) * pixelAngle / 60;

    fragment = texture(textureSampler, textureLocation);

    // Calculate Grating
    float angle = orientation*(M_PI/180) ;
	float grating = patch_test(x,y,r,width,angle,phase);
	float circle_gauss_1 =  circle(x,y,r,width);

	// Calculate Noise
	vec4 float_texture = vec4(texture(textureSampler, textureLocation).r, texture(textureSampler, textureLocation).g, texture(textureSampler, textureLocation).b, alpha);
	vec3 noise0 = float_texture.rgb; // range [0 1]
    vec3 noise1 = noiseAmp * ( 2.0f* (noise0 - 0.5f) ); // range noiseAmp*[-1 1]


   vec3 image1 = noise1 + (grating * signalAmp);
    // Define Image
   if (flag == 0)
   	{
   		image1 = noise1;
   	}
   	else if (flag == 1)
   	{
   		image1 = noise1 + (grating * signalAmp);
   	}

   	vec3 image0 = (image1 / 2.0f) + 0.5f; // range [0 1]
    fragment = vec4( image0, 1.0 ); // set output

}
