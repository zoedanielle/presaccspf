#include "presaccspf.hpp"

#include <eye/configuration.hpp>
#include <eye/messaging.hpp>

namespace user_tasks::presaccspf {

EYERIS_PLUGIN(presaccspf)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Public methods

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
presaccspf::presaccspf() :
    EyerisTask()
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::eventCommand(int command, const basic::types::JSONView& arguments)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::eventConfiguration(const presaccspfConfiguration::ptr_t& configuration)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::eventConsoleChange(const basic::types::JSONView& change)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::finalize()
{
    // Nothing to do
    endTrial();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::initialize()
{
    nTrialLim  = 200;
    pixelAngle = getAngleConverter()->pixel2ArcminH(1);

    totalCatch =0;
    totalTrials =0;
    totalFixLeft =0;
    totalFixRight =0;
    changeLeft =0;
    changeRight =0;

    //set session type
    SessionType = getConfiguration()->getSessionType();


    // Create gabor object
    orientation1 = getConfiguration()->getOrientation();
    FixSize = getConfiguration()->getFixSize();
    amplitude = getConfiguration()->getAmplitude();
    gaborSigma = getConfiguration()->getGaborSigma();
    //spatialFreq = getConfiguration()->getSpf();
    //gaborEdgeRadius = getConfiguration()->getGaborEdgeRadius();
    solidColorShader = std::make_shared<eye::graphics::TexturedSurfaceShader>();
    solidColorShader->load(fromAssetsManager()->inAssets("shaders/circle/textured_surface.vert"),
                           fromAssetsManager()->inAssets("shaders/circle/shader_gabor_noisyBitStealing.frag"));

    solidColorShader->use();
    solidColorShader->setUniform("spatialFreq", 0.0f);
    solidColorShader->setUniform("phase", 0.f);
    solidColorShader->setUniform("amplitude", amplitude);
    solidColorShader->setUniform("orientation", orientation1);
    solidColorShader->setUniform("pixelAngle", pixelAngle);
    solidColorShader->setUniform("screenOffsetX", sWidthPix/2);
    solidColorShader->setUniform("screenOffsetY", sHeightPix/2); // for graphic coordinate
    solidColorShader->setUniform("cX", 0); // center of the full-screen grating
    solidColorShader->setUniform("cY", 0); // center of the full-screen grating
    solidColorShader->setUniform("gaborX", 0); // center of the gaussian
    solidColorShader->setUniform("gaborY", 0); // center of the gaussian
    solidColorShader->setUniform("sigma", gaborSigma); // size of the gabor (not exact control)
    solidColorShader->setUniform("RB", (float)4);
    solidColorShader->setUniform("GB", (float)14);
    solidColorShader->setUniform("rndseed1", rand() % 10000);
    solidColorShader->setUniform("rndseed2", rand() % 10000);

    stimulus_solid = newTexturedPlane(sWidthPix, sHeightPix, std::vector<eye::graphics::RGB>(sWidthPix * sHeightPix,
                                                                                             eye::graphics::RGB::GRAY), solidColorShader);
    stimulus_solid->setPosition(0, 0);


    // boxes for the recalibration trials
    whitebox = newSolidPlane(FixSize/pixelAngle, FixSize/pixelAngle, eye::graphics::RGB(255, 255, 255));
    blackbox = newSolidPlane(FixSize/pixelAngle, FixSize/pixelAngle, eye::graphics::RGB(0,0,0));

    // fixation marker
    fixationMark = newSolidPlane(FixSize/pixelAngle, FixSize/pixelAngle, eye::graphics::RGB(0,0,0));
    fixationMark ->setPosition(0, 0);
    fixationMark ->hide();


    // performance keepers
        totalCorrect = 0;
        totalResponses = 0;


    // photocell image
    photoCell = newSolidPlane(80,80,eye::graphics::RGB(255, 255, 255)); //255 for white, 0 for black
    photoCell->setPosition(900, 470);
    photoCell->hide();

    Increment = 1;
    ResponseFinalize = 0;
    xshift = 0;
    yshift = 0;
    xPos = 0;
    yPos = 0;
    TrialNumber = 1;
    m_numTestCalibration = 0;

    // set TestCalibration = 1 so that the experiment will start with a recalibration trial
    TestCalibration = 1;

    hideAllObjects();
    m_state = STATE_LOADING;
    m_timer.start(1000ms);
    startTrial();
    WAIT_RESPONSE = 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::setup()
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::streamAnalog(const eye::signal::DataSliceAnalogBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::streamDigital(const eye::signal::DataSliceDigitalBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::streamEye(const eye::signal::DataSliceEyeBlock::ptr_t& data)
{

    storeUserStream("state", static_cast<int> (m_state));

    std::shared_ptr<eye::signal::DataSliceEye> slice = data->getLatest();

    float x;
    float y;

    switch (m_state) {
        case STATE_LOADING: {
            stimulus_solid->hide();

            if (m_timer.hasExpired()) {
                //info("Go To Fixation Called");
                gotoFixation();
            }

            break;

        }
        case STATE_TESTCALIBRATION: {
            stimulus_solid->hide();


            if (!m_timerCheck.hasExpired()) {

                x = getAngleConverter()->arcmin2PixelH(slice->calibrated1.x()) + xshift;
                y = getAngleConverter()->arcmin2PixelV(slice->calibrated1.y()) + yshift;


            } else {
                if (!(ResponseFinalize == 1)) {

                    x = getAngleConverter()->arcmin2PixelH(slice->calibrated1.x()) + xshift;
                    y = getAngleConverter()->arcmin2PixelV(slice->calibrated1.y()) + yshift;

                    blackbox->setPosition(x + xshift + xPos, y + yshift + yPos);
                    whitebox->setPosition(0, 0);

                    whitebox->show();
                    moveToFront(whitebox);
                    blackbox->show();
                    moveToFront(blackbox);

                } else {
                    info("\n Recalibration Submitted");
                    TestCalibration = 0;

                    xshift = xPos + xshift;
                    yshift = yPos + yshift;

                    blackbox->hide();
                    whitebox->hide();

                    recalsave["TrialNumber"] = TrialNumber;
                    recalsave["xshift"] = xshift;
                    recalsave["yshift"] = yshift;
                    storeUserVariable("recal" + int2string(TrialNumber) + "Data", recalsave);

                    gotoFixation();

                }

            }
            break;
        }
            case STATE_FIXATION: {
                // fixation dot to appear either on left or right of center;
                if (SessionType == 1) {
                    fixationMark->setPosition(fixationX,fixationY);
                    fixationMark->show();
                   // info("\n Enter State Fixation");

                } // in SessionType == 2 this object will be gabor patch
                else if (SessionType == 2) {

                    solidColorShader->use();
                    orientation1 = 0;
                    solidColorShader->setUniform("gaborX", gaborX); // center of the gaussian
                    solidColorShader->setUniform("gaborY", gaborY); // center of the gaussian
                    solidColorShader->setUniform("orientation", orientation1);
                    solidColorShader->setUniform("spatialFreq", spatialFreq);
                    solidColorShader->setUniform("phase", phase);
                    solidColorShader->dismiss();
                    stimulus_solid->show();
                    //info("\n Enter State Fixation");

                }

                // if eye is detected for 400 ms switch to STATE_STIMULUS
                if (gateEyePos == 1) {
                    m_timerEyePos.start(400ms);
                    gateEyePos = 0;
                }

                if (!m_timerEyePos.hasExpired()) {
                    std::shared_ptr<eye::signal::DataSliceEye> slice = data->getLatest();

                    x = getAngleConverter()->arcmin2PixelH(slice->calibrated1.x()) + xshift;
                    y = getAngleConverter()->arcmin2PixelV(slice->calibrated1.y()) + yshift;

                    float d = sqrt(pow(fixationX - x, 2) + pow(fixationY - y, 2));
//                    if (d > 20) {
//                        gateEyePos = 1;
//                        info("\n Fixation Timer Reset");
//
//                    }
                } else {

                    storeUserEvent("FrameStimulusON");
                    m_state = STATE_STIMULUS;
                    m_timerRamp.start(400ms);
                    info("\n Stimulus Begins (400ms)");

                }

                break;
            }
            case STATE_STIMULUS: {
                if (SessionType == 1) {

                    TimeStimulusON =  m_timerExp.getTime();

                    // re-enforce fixation position
                    fixationMark->setPosition(fixationX, fixationY);
                    fixationMark->show();

                    // set saccade landing position
                    solidColorShader->use();
                    orientation1 = 0;
//                    curr_ramp_time = m_timerRamp.getTime().count();
//                    float ramp_contrast = curr_ramp_time / ramp_dur.count();
//                    solidColorShader->setUniform("amplitude", 1 * ramp_contrast);
                    solidColorShader->setUniform("gaborX", gaborX); // center of the gaussian
                    solidColorShader->setUniform("gaborY", gaborY); // center of the gaussian
                    solidColorShader->setUniform("orientation", orientation1);
                    solidColorShader->setUniform("spatialFreq", spatialFreq);
                    solidColorShader->setUniform("phase", phase);
                    solidColorShader->dismiss();
                    stimulus_solid->show();
                    moveToFront(fixationMark);



                } else if (SessionType == 2) {

                    TimeStimulusON =  m_timerExp.getTime();

                    // re-enforce fixation position with gabor
                    solidColorShader->use();
                    orientation1 = 0;
//                    curr_ramp_time = m_timerRamp.getTime().count();
//                    float ramp_contrast = curr_ramp_time / ramp_dur.count();
//                    solidColorShader->setUniform("amplitude", 1 * ramp_contrast);
                    solidColorShader->setUniform("gaborX", gaborX); // center of the gaussian
                    solidColorShader->setUniform("gaborY", gaborY); // center of the gaussian
                    solidColorShader->setUniform("orientation", orientation1);
                    solidColorShader->setUniform("spatialFreq", spatialFreq);
                    solidColorShader->setUniform("phase", phase);
                    solidColorShader->dismiss();
                    stimulus_solid->show();


                }
                if (m_timerRamp.hasExpired()){

                    m_timerDelay.start(100ms);
                    info("\n Delay Timer Begins (50ms)");
                    m_state = STATE_CHANGE;
                    if (SessionType == 1){
                        fixationMark->hide();
                        moveToBack(fixationMark);
                        TimeFixationOFF =  m_timerExp.getTime();
                        storeUserEvent("FrameFixationOFF");

                    }
                    else if (SessionType == 2){

                        fixationMark->setPosition(fixationX, fixationY);
                        fixationMark->show();
                        moveToFront(fixationMark);
                        storeUserEvent("FrameFixationOFF");
                        TimeFixationOFF =  m_timerExp.getTime();

                    }
                }
                break;
            }
            case STATE_CHANGE: {
                //float d = sqrt(pow(fixationX - x, 2) + pow(fixationY - y, 2));

                if (!m_timerDelay.hasExpired()){
                    //info("\nWaiting for Delay Timer to End");

                    if(SessionType == 1) {
                        orientation1 = 0;
                        solidColorShader->use();
                        solidColorShader->setUniform("gaborX", gaborX); // center of the gaussian
                        solidColorShader->setUniform("gaborY", gaborY); // center of the gaussian
                        solidColorShader->setUniform("orientation", orientation1);
                        solidColorShader->setUniform("spatialFreq", spatialFreq);
                        solidColorShader->setUniform("phase", phase);
                        solidColorShader->dismiss();
                        stimulus_solid->show();
                        }
                        else if (SessionType == 2){
                        solidColorShader->use();
                        orientation1 = 0;
                        solidColorShader->setUniform("gaborX", gaborX); // center of the gaussian
                        solidColorShader->setUniform("gaborY", gaborY); // center of the gaussian
                        solidColorShader->setUniform("orientation", orientation1);
                        solidColorShader->setUniform("spatialFreq", spatialFreq);
                        solidColorShader->setUniform("phase", phase);
                        solidColorShader->dismiss();
                        stimulus_solid->show();
                        }
                    }

//                else if ((!m_timerDelay.hasExpired())){
//                    m_state = STATE_FIXATION;
//                    info("\nLeft STATE_CHANGE for STATE_FIXATION");
//                }
                else if (m_timerDelay.hasExpired()) {
                    info("\nDelay Time Ends");
                    m_timerChange.start(50ms);
                    m_state = STATE_RETURN;
                    WAIT_RESPONSE = 1;

                }
                break;
            }
            case STATE_RETURN: {
                if (!m_timerChange.hasExpired()) {
                    info("\nStimulus Change");

                        solidColorShader->use();
                    solidColorShader->setUniform("phase", phase);
                    solidColorShader->setUniform("gaborX", gaborX); // center of the gaussian
                        solidColorShader->setUniform("gaborY", gaborY); // center of the gaussian
                        solidColorShader->setUniform("orientation", orientation2);
                        solidColorShader->setUniform("spatialFreq", spatialFreq);
                        solidColorShader->dismiss();
                        stimulus_solid->show();
                        TimeChangeON =  m_timerExp.getTime();
                    storeUserEvent("FrameChangeON");


                } else if (m_timerChange.hasExpired()) {

                    m_timerResp.start(2000ms);
                    m_state = STATE_RESPONSE;

                }

                break;
            }
            case STATE_RESPONSE: {
                solidColorShader->use();
                orientation1 = 0;
                solidColorShader->setUniform("phase", phase);
                solidColorShader->setUniform("gaborX", gaborX); // center of the gaussian
                solidColorShader->setUniform("gaborY", gaborY); // center of the gaussian
                solidColorShader->setUniform("orientation", orientation1);
                solidColorShader->setUniform("spatialFreq", spatialFreq);
                solidColorShader->dismiss();
                stimulus_solid->show();
                TimeChangeOFF =  m_timerExp.getTime();
                storeUserEvent("FrameChangeOFF");

                if (SessionType == 2){
                    fixationMark->show();
                    moveToFront(fixationMark);
                }

                if (((m_timerResp.hasExpired())) || (WAIT_RESPONSE == 0)) {

                    saveData();
                    if (WAIT_RESPONSE == 1) {
                        info("\nResponse not given");

                    }


                }
                break;

            }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::streamKeyboard(const eye::signal::DataSliceKeyboardBlock::ptr_t& data)
{
    auto keyboard = data->getLatest();
//    if (m_state == STATE_FIXATION){
//        if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_SPACE))  // moving the cursor up
//        {
//            m_state = STATE_STIMULUS;
//        }
//    }
//    if (m_state == STATE_CHANGE){
//        if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_SPACE))  // moving the cursor up
//        {
//            m_state = STATE_RETURN;
//
//        }
//    }
    if (m_state == STATE_TESTCALIBRATION)
    {

        if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_w))  // moving the cursor up
        {
            yPos = yPos + Increment; //position of the cross
        }

        else if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_s))  // moving the cursor down
        {
            yPos = yPos - Increment;
        }

        else if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_d))// moving the cursor to the right
        {
            xPos = xPos + Increment;

        }

        else if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_a)) // moving the cursor to the left
        {
            xPos = xPos - Increment;

        }

        if (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_x)) // finalize the response
        {
            info("Recalibration finalized");
            ResponseFinalize = 1;

        }
    }
    if (m_state == STATE_RESPONSE)
    {
        if ( (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_1)) |
             (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_2)) )
        {
            WAIT_RESPONSE = 0;
            totalResponses++;

            // get the time of the response here
            ResponseTime =  m_timerExp.getTime();

            // right press for rightward
            if ( (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_2)) )// target tilted R
            {
                info("\nSubject's Response: Right");
                Response = -45;
            }

            if ( (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_1))  )// target tilted L
            {
                info("\nSubject's Response: Left");
                Response = 45;
            }


            if (TargetOrientation == Response)
            {
                Correct = 1;
                totalCorrect++;
                info("Correct\n");
            }
            else if (TargetOrientation != Response && TargetOrientation  != 0)
            {
                Correct = 0;
                info("Wrong\n");
            }
            else if (TargetOrientation  == 0){
                info("Catch Trial\n");
                totalCatch++;
            }


            info("Prop Correct:" + std::to_string(totalCorrect/ (totalResponses - totalCatch)));

        }


    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::streamJoypad(const eye::signal::DataSliceJoypadBlock::ptr_t& data)
{
// activate the joypad only in the state calibration
        auto joypad = data->getLatest();

        if (m_state == STATE_TESTCALIBRATION)
        {

            if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_UP))  // moving the cursor up
            {
                yPos = yPos + Increment; //position of the cross
            }

            else if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_DOWN)) // moving the cursor down
            {
                yPos = yPos - Increment;
            }

            else if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_RIGHT)) // moving the cursor to the right
            {
                xPos = xPos + Increment;

            }

            else if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_LEFT)) // moving the cursor to the left
            {
                xPos = xPos - Increment;

            }

            if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_TRIANGLE)) // finalize the response
            {
                info("Recalibration finalized");
                ResponseFinalize = 1; // click the left botton to finalize the response

            }
        }
        if (m_state == STATE_RESPONSE)
        {
            if ( (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_R1)) |
                 (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_L1)) )
            {
                WAIT_RESPONSE = 0;
                totalResponses++;

                // get the time of the response here
                ResponseTime =  m_timerExp.getTime();

                // right press for rightward
                if ( (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_R1)) )// target tilted R
                {
                    //info("\nSubject's Response: Right");
                    Response = -45;
                }

                if ( (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_L1)) )// target tilted L
                {
                    //info("\nSubject's Response: Left");
                    Response = 45;
                }

                if (TargetOrientation == Response)
                {
                    Correct = 1;
                    totalCorrect++;
                    info("Correct\n");
                }
                else if (TargetOrientation != Response && TargetOrientation  != 0)
                {
                    Correct = 0;
                    info("Wrong\n");
                }
                else if (TargetOrientation  == 0){
                    info("Catch Trial\n");
                    totalCatch++;
                }


                info("Prop Correct:" + std::to_string(totalCorrect/ (totalResponses - totalCatch)));
                ;
            }


        }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::streamMonitor(const eye::signal::DataSliceMonitorBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::streamMouse(const eye::signal::DataSliceMouseBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::streamVideoCard(const eye::signal::DataSliceVideoCardBlock::ptr_t& data)
{
    // Nothing to do
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::detectEye(const eye::signal::DataSliceEyeBlock::ptr_t& data) {
    float x;
    float y;

    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::gotoFixation() {

        if (!(TestCalibration == 1))

            info("Trial Number: " + std::to_string(TrialNumber));

        hideAllObjects();

        if (TestCalibration == 1){

            if (TrialNumber > nTrialLim) {
                finalize();
            }

            m_timerCheck.start(100ms);
            info("Beginning Recalibration");
            m_state = STATE_TESTCALIBRATION;
            ResponseFinalize = 0;
        }

        else{

            TimeFixationON =  m_timerExp.getTime();
            storeUserEvent("FrameFixationON");
            m_state = STATE_FIXATION;
            gate = 1;
            gateEyePos = 1;

            // if the response is not given Correct is set at 10
            Correct = 10;

            // Determine orientation
            int orichoices[3] = { 45, -45, 0 }; // left, right, none // 0,1,2
            int freq[] = {45,45,10};
            int n = sizeof(orichoices) / sizeof(orichoices[0]);
            TargetOrientation =  myRand(orichoices, freq, n);
            orientation2 = TargetOrientation;

            // Determine spatial frequencies
            float spfchoices[4] = {4.0f, 6.0f, 8.0f, 12.0f};
            int spf_id = rand() % 4;
            spatialFreq = spfchoices[spf_id];

            // Determine phase
            float phasechoices[4] = {0.0f,90.0f,180.0f,270.0f};
            int phase_id = rand() % 4;
            phase = phasechoices[phase_id];

            // Determine fixation and stimulus positions
            // Determine fixation and stimulus positions
            if (SessionType == 1 ) {

                // Determine fixation location
                int arr[] = { 0, 1}; // 0 - left side, 1 - right side
                int freq[] = {50,50};
                int n = sizeof(arr) / sizeof(arr[0]);
                fixLoc = (rand() % n); //calculating equal frequency across conditions

                // Determine stimulus location
                if (fixLoc == 0){
                    gaborX =  (rand() % 10 + 15)/pixelAngle; // range in arcmin
                    gaborY =  (rand() % 5 + 0)/pixelAngle; // range in arcmin
                    fixationX = 0; // arcmin
                    fixationY = 0; // arcmin
                }
                else if (fixLoc == 1){
                    gaborX =  (rand() % -10 + -15)/pixelAngle; // range in arcmin
                    gaborY =  (rand() % 5 + 0)/pixelAngle; // range in arcmin
                    fixationX = 0; // arcmin
                    fixationY = 0; // arcmin
                }
            }
            else if (SessionType == 2){

                // Determine fixation location
                int arr[] = { 0, 1}; // 0 - left side, 1 - right side
                int freq[] = {50,50};
                int n = sizeof(arr) / sizeof(arr[0]);
                fixLoc = (rand() % n); //calculating equal frequency across conditions

                // Determine stimulus location
                if (fixLoc == 0){
                    gaborX =  0; // range in arcmin
                    gaborY =  0; // range in arcmin
                    fixationX = (rand() % 10 + 15)/pixelAngle ; // arcmin
                    fixationY = (rand() % 5 + 0)/pixelAngle; // arcmin
                }
                else if (fixLoc == 1){
                    gaborX =  0; // range in arcmin
                    gaborY =  0; // range in arcmin
                    fixationX = (rand() % -10 + -15)/pixelAngle ; // arcmin
                    fixationY = (rand() % 5 + 0)/pixelAngle; // arcmin
                }
            }

            // reset gabor object
            solidColorShader->use();
            solidColorShader->setUniform("phase", phase);
            solidColorShader->setUniform("amplitude", amplitude);
            solidColorShader->setUniform("gaborX", gaborX); // center of the gaussian
            solidColorShader->setUniform("gaborY", gaborY); // center of the gaussian
            solidColorShader->setUniform("orientation", orientation1);
            solidColorShader->setUniform("spatialFreq", spatialFreq);
            solidColorShader->dismiss();

            // reset fixation dot
            fixationMark ->setPosition(fixationX, fixationY);
            fixationMark->hide();

            // start the trial
            photoCell->setColor(eye::graphics::RGB(255, 255, 255));//White
            photoCell->hide();

            WAIT_RESPONSE = 1;
            m_timerExp.start(10000ms);
            m_timer.start(1000ms);

            gate = 1;
            gateEyePos = 1;
            info("Orientation" + std::to_string(orientation2));
            info("Spf" + std::to_string(spatialFreq));
            info("Location" + std::to_string(gaborX));
            info("Phase" + std::to_string(phase));


        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void presaccspf::saveData() {

        // ---- Saved each protocol run -------
        storeUserVariable("SessionType", SessionType);
        storeUserVariable("pixelAngle", pixelAngle);


        // ---- Saved every trial -------
        trialDatasave["Correct"] = Correct;
        trialDatasave["TargetOrientation"] = TargetOrientation;
        trialDatasave["Response"] = Response;

        // fixation marker size
        trialDatasave["FixSize"] = FixSize; //px

        // position of fixation dot
        trialDatasave["FixLocX"] = fixationX;
        trialDatasave["FixLocY"] = fixationY;

        // stimulus params
        trialDatasave["Orientation"] = orientation2;
        trialDatasave["Spf"] = spatialFreq;
        trialDatasave["GaborAmp"] = amplitude;
        trialDatasave["GaborLocX"] = gaborX;
        trialDatasave["GaborLocY"] = gaborY;
        trialDatasave["GaborSigma"] = gaborSigma;

        // events
        trialDatasave["ResponseTime"] = ResponseTime.count();
        trialDatasave["TimeFixationON"] = TimeFixationON.count();
        trialDatasave["TimeFixationOFF"] = TimeFixationOFF.count();
        trialDatasave["TimeStimulusON"] = TimeStimulusON.count();
        trialDatasave["TimeChangeON"] = TimeChangeON.count();
        trialDatasave["TimeChangeOFF"] = TimeChangeOFF.count();


        storeUserVariable("trial" + int2string(TrialNumber) + "Data",trialDatasave);


        // keep track of the test calibration trials
        m_numTestCalibration++;

        // recalibration active at each trial (here set at 1)
        if (m_numTestCalibration == 1) {
            xPos = 0;
            yPos = 0;
            TestCalibration = 1;
            ResponseFinalize = 0;
            m_numTestCalibration = 0;
            whitebox->setPosition(0, 0);
            whitebox->show();
        }

        m_timerExp.reset();
        TrialNumber++;
        info("-----------------------------------------------------");
        gotoFixation();



    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void presaccspf::teardown()
{
    // Nothing to do
}
    int presaccspf::findCeil(std::vector<int> arr, int r, int l, int h) {
        int mid;
        while (l < h) {
            mid = l + ((h - l) >> 1);  // Same as mid = (l+h)/2
            (r > arr[mid]) ? (l = mid + 1) : (h = mid);
        }
        return (arr[l] >= r) ? l : -1;

    }
    int presaccspf::myRand(int *arr, int *freq, int n) {
        srand(time(NULL));
        // Create and fill prefix array
        std::vector<int> prefix(n, 0);
        int i;
        prefix[0] = freq[0];
        for (i = 1; i < n; ++i)
            prefix[i] = prefix[i - 1] + freq[i];

        // prefix[n-1] is sum of all frequencies. Generate a random number
        // with value from 1 to this sum
        int r = (rand() % prefix[n - 1]) + 1;

        // Find index of ceiling of r in prefix arrat
        int indexc = findCeil(prefix, r, 0, n - 1);
        return arr[indexc];

    }

    std::string presaccspf::int2string(int x) {
        std::stringstream temps;
        temps << x;
        return temps.str();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Protected methods

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private methods

}  // namespace user_tasks::presaccspf
