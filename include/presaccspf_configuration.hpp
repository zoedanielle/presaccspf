//
// Copyright (c) 2017-2020 Santini Designs. All rights reserved.
//

#pragma once

#include <basic/types.hpp>

#include <eye/protocol.hpp>

namespace user_tasks::presaccspf {

/**
 * The welcome banner configuration class is configuration class for the welcome banner task.
 */
struct presaccspfConfiguration : public eye::protocol::EyerisTaskConfiguration
{
    using ptr_t = std::shared_ptr<presaccspfConfiguration>;  ///< Pointer type definition for the class

    /**
     * @brief Static factory.
     *
     * @param[in] other Pointer to another JSON object that defines the initial schema of this one.
     *
     * @return Pointer to a new class instance
     */
    [[maybe_unused]]
    static ptr_t factory_ptr(const basic::types::JSON::sptr_t& other)
    {
        return std::make_shared<presaccspfConfiguration>(other);
    }

    /**
     * @brief Constructor that instantiate the configuration from the prototype, but then let set the values
     * also based on the parent configuration.
     *
     * @param[in] other Pointer to a JsonObject used to initialize the configuration
     */
    explicit presaccspfConfiguration(basic::types::JSON::sptr_t other) :
        EyerisTaskConfiguration(other)
    {
        //initializeSpf(2.5f); // in cycles per degree
        initializeAmplitude(0.50f); // controls difficulty
        initializeOrientation(0.0f); // in degrees
        initializeGaborSigma(2.5f); // size of gabor in arcmin
        //initializeGaborEdgeRadius(5.0f); // radius of mask edge?
        initializeSessionType(1); // 1 - test saccade goal, 2 - test COG
        initializeFixSize(3); // in arcmin


    }
    //LC_PROPERTY_FLOAT(SPF, "0: spatialFreq", Spf);
    LC_PROPERTY_FLOAT(AMPLITUDE, "0: amplitude", Amplitude);
    LC_PROPERTY_FLOAT(ORIENTATION, "0: orientation", Orientation);
    LC_PROPERTY_FLOAT(GABORSIGMA, "0: gaborSigma", GaborSigma);
    //LC_PROPERTY_FLOAT(GABOREDGERADIUS,"0: gaborEdgeRadius",GaborEdgeRadius);
    LC_PROPERTY_FLOAT(SESSIONTYPE,"0: sessionType",SessionType);
    LC_PROPERTY_FLOAT(FIXSIZE,"0: fixSize",FixSize);



};

}  // namespace user_tasks::presaccspf
